# 实验4：PL/SQL语言打印杨辉三角

姓名：龚亮 学号：202010414404

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

认真阅读并运行下面的杨辉三角源代码。
将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
运行这个存储过程即可以打印出N行杨辉三角。
写出创建YHTriange的SQL语句。

## 杨辉三角源代码


```python
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/

```


```python
代码使用了一个数组 rowArray，该数组存储杨辉三角中每一行的数字，并在每一行结束后打印出来。下面是代码的详细分析：

type t_number is varray (100) of integer not null;
定义了一个类型 t_number，该类型是一个大小为 100 的整数数组。

spaces varchar2(30) :=' ';
定义了一个变量 spaces，用于在打印数字时分隔每一个数字。

N integer := 9;
定义了一个变量 N，指定了杨辉三角形的行数。

rowArray t_number := t_number();
声明并初始化了一个 t_number 类型的数组 rowArray。

dbms_output.put_line('1');
先输出杨辉三角形的第一行。

dbms_output.put(rpad(1,9,' '));
先输出杨辉三角形的第二行的第一个数字（数字 1）。

dbms_output.put(rpad(1,9,' '));
先输出杨辉三角形的第二行的第二个数字（数字 1）。

dbms_output.put_line('');
输出一个换行符，换行到下一行。

for i in 1 .. N loop rowArray.extend; end loop;
用 extend 方法初始化 rowArray 数组的大小为 N，每个元素的值为 0。

rowArray(1):=1; rowArray(2):=1;
将 rowArray 数组的第一个和第二个元素赋值为 1。

for i in 3 .. N loop
从第三行开始循环每一行。

rowArray(i):=1;
将当前行的最后一个数字赋值为 1。

while j>1 loop rowArray(j):=rowArray(j)+rowArray(j-1); j:=j-1; end loop;
计算当前行的每个数字的值，从第二个数字开始到倒数第二个数字，每个数字的值为其上一行相邻两个数字之和。

for j in 1 .. i loop dbms_output.put(rpad(rowArray(j),9,' ')); end loop;
打印当前行的所有数字，使用 rpad 函数在数字前面填充空格，保证每个数字占据相同的宽度。

dbms_output.put_line('');
打印一个换行符，换行到下一行。

总之，这段代码使用数组和循环语句计算和打印了一个杨辉三角形。
```

## 建立存储代码

CREATE OR REPLACE PROCEDURE YANGHUI(N IN integer)
AS
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;

![](1.png)

![](2.png)

本次实验要求将已有的PL/SQL源代码转化为存储过程，并能够根据用户输入的参数打印出相应行数的杨辉三角。通过这次实验，我深入了解了存储过程的编写和使用方法，同时也加深了对PL/SQL语言和组合数的理解。在实验过程中，我遇到了一些问题，例如在创建存储过程时需要注意参数的类型和默认值等细节，需要仔细阅读文档并进行实践。通过反复测试和调试，我成功地完成了本次实验，并对PL/SQL和存储过程有了更深入的理解。
