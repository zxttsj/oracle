 # 实验2：用户及权限管理

学号：202010414404 姓名：龚亮

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤


```python

$ sqlplus system/123@pdborcl
CREATE ROLE con_res_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
--收回角色
REVOKE con_res_role FROM sale;
```

sqlplus system/123@pdborcl: 这是使用 SQL*Plus 工具连接到 pdborcl 数据库实例的命令，其中 system 是用户名，123 是密码，@pdborcl 表示连接到 pdborcl 实例。

CREATE ROLE con_res_role;: 这是创建一个名为 con_res_role 的角色。

GRANT connect,resource,CREATE VIEW TO con_res_role;: 这是将 connect、resource 和 CREATE VIEW 权限授予给 con_res_role 角色。

CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;: 这是创建一个名为 sale 的用户，该用户的密码为 123，默认表空间为 users，临时表空间为 temp。

ALTER USER sale default TABLESPACE "USERS";: 这是将 sale 用户的默认表空间修改为 USERS。

ALTER USER sale QUOTA 50M ON users;: 这是将 sale 用户在 users 表空间上的配额限制为 50M。

GRANT con_res_role TO sale;: 这是将 con_res_role 角色授予给 sale 用户。




```python
$ sqlplus sale/123@pdborcl
SQL> show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang

```

代码主要是使用 SQL*Plus 工具连接到 pdborcl 数据库实例中的 sale 用户，并进行了一系列的操作，包括创建表、插入数据、创建视图、授予权限等。具体分析如下：

sqlplus sale/123@pdborcl: 这是使用 SQL*Plus 工具连接到 pdborcl 数据库实例中的 sale 用户，其中 sale 是用户名，123 是密码，@pdborcl 表示连接到 pdborcl 实例。

SQL> show user;: 这是在 SQL*Plus 工具中执行的命令，用于显示当前用户，结果显示当前用户为 sale。

SQL> SELECT * FROM session_privs;: 这是在 SQL*Plus 工具中执行的命令，用于显示当前用户的权限，结果为空。

SQL> SELECT * FROM session_roles;: 这是在 SQL*Plus 工具中执行的命令，用于显示当前用户所属的角色，结果为空。

SQL> CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;: 这是创建一个名为 customers 的表，包括 id 和 name 两个列，表空间为 USERS。

SQL> INSERT INTO customers(id,name)VALUES(1,'zhang');: 这是向 customers 表中插入一条记录，该记录的 id 为 1，name 为 zhang。

SQL> INSERT INTO customers(id,name)VALUES (2,'wang');: 这是向 customers 表中插入一条记录，该记录的 id 为 2，name 为 wang。

SQL> CREATE VIEW customers_view AS SELECT name FROM customers;: 这是创建一个名为 customers_view 的视图，该视图从 customers 表中选择 name 列。

SQL> GRANT SELECT ON customers_view TO hr;: 这是将 SELECT 权限授予给 hr 用户，以便其可以查询 customers_view 视图。

SQL> SELECT * FROM customers_view;: 这是查询 customers_view 视图，并将结果输出，结果显示了 customers 表中的两条记录中的 name 列。

总的来说，这段代码主要是创建了一个名为 customers 的表，插入了两条记录，创建了一个名为 customers_view 的视图，并将 SELECT 权限授予给 hr 用户，最后查询了 customers_view 视图并输出了结果。


```python
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers;
elect * from sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM sale.customers_view;
NAME
--------------------------------------------------
zhang
wang

```

这段代码是使用 SQL*Plus 工具连接到 pdborcl 数据库实例中的 hr 用户，并进行了一系列操作，包括查询 sale.customers 和 sale.customers_view。具体分析如下：

sqlplus hr/123@pdborcl: 这是使用 SQL*Plus 工具连接到 pdborcl 数据库实例中的 hr 用户，其中 hr 是用户名，123 是密码，@pdborcl 表示连接到 pdborcl 实例。

SQL> SELECT * FROM sale.customers;: 这是查询 sale 用户下的 customers 表，结果显示错误 ORA-00942: 表或视图不存在，这是因为 sale 用户没有授予 hr 用户查询 customers 表的权限。

SQL> SELECT * FROM sale.customers_view;: 这是查询 sale 用户下的 customers_view 视图，结果显示了 customers_view 视图中的两条记录，这是因为 sale 用户已经将 SELECT 权限授予给了 hr 用户，以便其可以查询 customers_view 视图。

综上，由于 sale 用户没有将 customers 表授予 hr 用户的查询权限，导致 hr 用户查询 customers 表时出现错误；而查询 customers_view 视图则正常，因为 sale 用户已经将 SELECT 权限授予给了 hr 用户。


```python
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;

```

这段代码是使用 SQL*Plus 工具连接到 pdborcl 数据库实例中的 system 用户，并进行了一系列操作，包括修改默认 PROFILE 中的 FAILED_LOGIN_ATTEMPTS 参数，以及解锁 sale 用户的账户。具体分析如下：

SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;: 这是修改默认的 PROFILE 中的 FAILED_LOGIN_ATTEMPTS 参数，将其设置为 3。这个参数指定了用户可以尝试登录但不成功的最大次数，超过此次数将会导致账户被锁定。

SQL> alter user sale account unlock;: 这是解锁 sale 用户的账户。由于 sale 用户之前可能登录失败了多次，导致其账户被锁定，需要通过此命令将其账户解锁才能进行正常的登录操作。

综上，这段代码的作用是修改默认的 PROFILE 中的 FAILED_LOGIN_ATTEMPTS 参数，以及解锁 sale 用户的账户，以保证用户可以正常登录到数据库实例中。


```python
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;

```

使用 SQL*Plus 工具连接到 pdborcl 数据库实例中的 system 用户，并进行了两个查询操作，分别是查询 USERS 表空间中的数据文件信息以及查询所有表空间的使用情况。具体分析如下：

SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files WHERE tablespace_name='USERS';: 这个语句查询了 USERS 表空间中的数据文件信息，其中使用了 dba_data_files 视图。这个视图显示了数据库中所有数据文件的信息，包括文件名、表空间名、文件大小、最大大小和是否可以自动扩展等。WHERE 子句指定了只查询表空间名为 USERS 的数据文件信息。

SQL> SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",Round(( total - free )/ total,4)* 100 "使用率%" from (SELECT tablespace_name,Sum(bytes)free FROM dba_free_space group BY tablespace_name)a,(SELECT tablespace_name,Sum(bytes)total FROM dba_data_files group BY tablespace_name)b where a.tablespace_name = b.tablespace_name;: 这个语句查询了所有表空间的使用情况，其中使用了 dba_free_space 和 dba_data_files 两个视图。dba_free_space 视图显示了数据库中所有表空间的空闲空间信息，包括表空间名和空闲空间大小；dba_data_files 视图显示了数据库中所有数据文件的信息，包括表空间名和文件大小等。这个查询通过连接两个视图，计算出了每个表空间的大小、剩余空间、使用空间和使用率等信息。

综上，这段代码的作用是查询 USERS 表空间中的数据文件信息以及查询所有表空间的使用情况，以便进行数据库空间管理和监控。


```python
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;

```

使用 SQL*Plus 工具连接到 pdborcl 数据库实例中的 system 用户，并进行了两个操作，分别是删除 con_res_role 角色和删除 sale 用户及其所有对象。具体分析如下：

SQL> drop role con_res_role;: 这个语句删除了 con_res_role 角色。DROP ROLE 命令用于删除数据库中的角色，这会将所有被赋予该角色的权限和访问控制都一并删除。

SQL> drop user sale cascade;: 这个语句删除了 sale 用户及其所有对象，包括表、视图、索引、序列等。DROP USER 命令用于删除数据库中的用户，CASCADE 关键字表示级联删除，即删除该用户所拥有的所有对象。

综上，这段代码的作用是删除数据库中的 con_res_role 角色和 sale 用户及其所有对象，以便进行数据库对象管理和维护。

## 结论

  本次实验通过创建一个新的本地角色con_res_role并授予connect、resource和CREATE VIEW权限，成功实现了让拥有该角色的用户拥有这三种权限的目的。同时，我们还创建了一个新用户sale，并给该用户分配表空间和限额，授予了con_res_role角色，并通过测试，验证了该角色的权限是否正确。这些操作均是Oracle数据库中常见的权限管理操作，对于开发人员和数据库管理员来说都是必备的基本操作。


```python

```
