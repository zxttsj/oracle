# 实验1：SQL语句的执行计划分析与优化指导

学号：202010414404 姓名：龚亮

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 查询

### 查询并输出平均薪资最高的部门ID并且还要输出这个部门中薪资最高的员工ID和他的薪资和所在城市，请用两种方式写Oracle数据库的sql查询。

### 方式一


```python
SELECT 
  d.DEPARTMENT_ID, 
  MAX(e.EMPLOYEE_ID) AS HIGHEST_EMPLOYEE_ID, 
  MAX(e.SALARY) AS HIGHEST_SALARY, 
  l.CITY
FROM 
  EMPLOYEES e 
  INNER JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID 
  INNER JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID 
GROUP BY 
  d.DEPARTMENT_ID, 
  l.CITY 
HAVING 
  AVG(e.SALARY) = (
    SELECT MAX(avg_salary)
    FROM (
      SELECT AVG(SALARY) AS avg_salary
      FROM EMPLOYEES
      GROUP BY DEPARTMENT_ID
    )
  );
```

### 代码分析

此 SQL 代码从三个表（EMPLOYEES、DEPARTMENTS 和 LOCATIONS）中检索数据，并对员工数据执行一组聚合函数。此代码检索有关特定城市每个部门中收入最高的员工和薪水的数据，同时还过滤结果以仅包括所有部门中平均薪水最高的部门。

### 运行信息分析


```python
DEPARTMENT_ID HIGHEST_EMPLOYEE_ID HIGHEST_SALARY CITY
------------- ------------------- -------------- ------------------------------
	   90		      102	   24000 Seattle


执行计划
----------------------------------------------------------
Plan hash value: 1241347288

--------------------------------------------------------------------------------
---------------

| Id  | Operation		   | Name	      | Rows  | Bytes | Cost (%C
PU)| Time     |

--------------------------------------------------------------------------------
---------------

|   0 | SELECT STATEMENT	   |		      |     2 |    60 |     8  (
13)| 00:00:01 |

|*  1 |  FILTER 		   |		      |       |       |
   |	      |

|   2 |   HASH GROUP BY 	   |		      |     2 |    60 |     8  (
13)| 00:00:01 |

|*  3 |    HASH JOIN		   |		      |   106 |  3180 |     7
(0)| 00:00:01 |

|*  4 |     HASH JOIN		   |		      |    27 |   513 |     4
(0)| 00:00:01 |

|   5 |      VIEW		   | index$_join$_004 |    23 |   276 |     2
(0)| 00:00:01 |

|*  6 |       HASH JOIN 	   |		      |       |       |
   |	      |

|   7 |        INDEX FAST FULL SCAN| LOC_CITY_IX      |    23 |   276 |     1
(0)| 00:00:01 |

|   8 |        INDEX FAST FULL SCAN| LOC_ID_PK	      |    23 |   276 |     1
(0)| 00:00:01 |

|   9 |      VIEW		   | index$_join$_002 |    27 |   189 |     2
(0)| 00:00:01 |

|* 10 |       HASH JOIN 	   |		      |       |       |
   |	      |

|  11 |        INDEX FAST FULL SCAN| DEPT_ID_PK       |    27 |   189 |     1
(0)| 00:00:01 |

|  12 |        INDEX FAST FULL SCAN| DEPT_LOCATION_IX |    27 |   189 |     1
(0)| 00:00:01 |

|  13 |     TABLE ACCESS FULL	   | EMPLOYEES	      |   107 |  1177 |     3
(0)| 00:00:01 |

|  14 |   SORT AGGREGATE	   |		      |     1 |    13 |
   |	      |

|  15 |    VIEW 		   |		      |    11 |   143 |     4  (
25)| 00:00:01 |

|  16 |     SORT GROUP BY	   |		      |    11 |    77 |     4  (
25)| 00:00:01 |

|  17 |      TABLE ACCESS FULL	   | EMPLOYEES	      |   107 |   749 |     3
(0)| 00:00:01 |

--------------------------------------------------------------------------------
---------------


Predicate Information (identified by operation id):
---------------------------------------------------

   1 - filter(SUM("E"."SALARY")/COUNT("E"."SALARY")= (SELECT MAX("AVG_SALARY") F
ROM

	      (SELECT SUM("SALARY")/COUNT("SALARY") "AVG_SALARY" FROM "EMPLOYEES
" "EMPLOYEES" GROUP

	      BY "DEPARTMENT_ID") "from$_subquery$_006"))
   3 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
   4 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
   6 - access(ROWID=ROWID)
  10 - access(ROWID=ROWID)

Note
-----
   - this is an adaptive plan


统计信息
----------------------------------------------------------
	440  recursive calls
	  0  db block gets
	801  consistent gets
	 24  physical reads
	  0  redo size
	812  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 45  sorts (memory)
	  0  sorts (disk)
	  1  rows processed
```

### 方式二


```python
WITH avg_salary_by_dept AS (
  SELECT 
    e.DEPARTMENT_ID, 
    AVG(e.SALARY) AS avg_salary
  FROM 
    EMPLOYEES e 
  GROUP BY 
    e.DEPARTMENT_ID
),
highest_salary_by_dept AS (
  SELECT 
    e.DEPARTMENT_ID, 
    MAX(e.SALARY) AS highest_salary,
    MAX(e.EMPLOYEE_ID) KEEP (DENSE_RANK FIRST ORDER BY e.SALARY DESC) AS highest_employee_id
  FROM 
    EMPLOYEES e 
  GROUP BY 
    e.DEPARTMENT_ID
)
SELECT 
  d.DEPARTMENT_ID, 
  hsb.highest_employee_id, 
  hsb.highest_salary, 
  l.CITY
FROM 
  DEPARTMENTS d 
  INNER JOIN avg_salary_by_dept asb ON d.DEPARTMENT_ID = asb.DEPARTMENT_ID 
  INNER JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID 
  INNER JOIN highest_salary_by_dept hsb ON d.DEPARTMENT_ID = hsb.DEPARTMENT_ID AND hsb.highest_salary = (
    SELECT MAX(highest_salary)
    FROM highest_salary_by_dept
  )
WHERE 
  asb.avg_salary = (
    SELECT MAX(avg_salary)
    FROM avg_salary_by_dept
  );
```

### 代码分析

这段代码通过多个子查询和内连接检索了部门、员工和位置等数据，并使用聚合函数计算了每个部门的平均工资和最高工资。代码最终输出了在平均工资最高的部门中，每个部门的最高薪资和最高薪资的员工的ID以及所在城市。


### 运行信息代码


```python
DEPARTMENT_ID HIGHEST_EMPLOYEE_ID HIGHEST_SALARY CITY
------------- ------------------- -------------- ------------------------------
	   90		      100	   24000 Seattle


执行计划
----------------------------------------------------------
Plan hash value: 1583722651

--------------------------------------------------------------------------------
--------------------------------------

| Id  | Operation				 | Name 		     | R
ows  | Bytes | Cost (%CPU)| Time     |

--------------------------------------------------------------------------------
--------------------------------------

|   0 | SELECT STATEMENT			 |			     |
   9 |	 756 |	  20  (10)| 00:00:01 |

|   1 |  TEMP TABLE TRANSFORMATION		 |			     |
     |	     |		  |	     |

|   2 |   LOAD AS SELECT (CURSOR DURATION MEMORY)| SYS_TEMP_0FD9D6610_27D759 |
     |	     |		  |	     |

|   3 |    HASH GROUP BY			 |			     |
  11 |	  77 |	   4  (25)| 00:00:01 |

|   4 |     TABLE ACCESS FULL			 | EMPLOYEES		     |
 107 |	 749 |	   3   (0)| 00:00:01 |

|   5 |   LOAD AS SELECT (CURSOR DURATION MEMORY)| SYS_TEMP_0FD9D6611_27D759 |
     |	     |		  |	     |

|   6 |    SORT GROUP BY			 |			     |
  11 |	 121 |	   4  (25)| 00:00:01 |

|   7 |     TABLE ACCESS FULL			 | EMPLOYEES		     |
 107 |	1177 |	   3   (0)| 00:00:01 |

|*  8 |   HASH JOIN				 |			     |
   9 |	 756 |	   8   (0)| 00:00:01 |

|*  9 |    HASH JOIN				 |			     |
   9 |	 648 |	   6   (0)| 00:00:01 |

|* 10 |     HASH JOIN				 |			     |
  10 |	 330 |	   4   (0)| 00:00:01 |

|* 11 |      VIEW				 |			     |
  11 |	 286 |	   2   (0)| 00:00:01 |

|  12 |       TABLE ACCESS FULL 		 | SYS_TEMP_0FD9D6610_27D759 |
  11 |	  77 |	   2   (0)| 00:00:01 |

|  13 |       SORT AGGREGATE			 |			     |
   1 |	  13 |		  |	     |

|  14 |        VIEW				 |			     |
  11 |	 143 |	   2   (0)| 00:00:01 |

|  15 | 	TABLE ACCESS FULL		 | SYS_TEMP_0FD9D6610_27D759 |
  11 |	  77 |	   2   (0)| 00:00:01 |

|  16 |      VIEW				 | index$_join$_003	     |
  27 |	 189 |	   2   (0)| 00:00:01 |

|* 17 |       HASH JOIN 			 |			     |
     |	     |		  |	     |

|  18 |        INDEX FAST FULL SCAN		 | DEPT_ID_PK		     |
  27 |	 189 |	   1   (0)| 00:00:01 |

|  19 |        INDEX FAST FULL SCAN		 | DEPT_LOCATION_IX	     |
  27 |	 189 |	   1   (0)| 00:00:01 |

|* 20 |     VIEW				 |			     |
  11 |	 429 |	   2   (0)| 00:00:01 |

|  21 |      TABLE ACCESS FULL			 | SYS_TEMP_0FD9D6611_27D759 |
  11 |	 121 |	   2   (0)| 00:00:01 |

|  22 |      SORT AGGREGATE			 |			     |
   1 |	  13 |		  |	     |

|  23 |       VIEW				 |			     |
  11 |	 143 |	   2   (0)| 00:00:01 |

|  24 |        TABLE ACCESS FULL		 | SYS_TEMP_0FD9D6611_27D759 |
  11 |	 121 |	   2   (0)| 00:00:01 |

|  25 |    VIEW 				 | index$_join$_006	     |
  23 |	 276 |	   2   (0)| 00:00:01 |

|* 26 |     HASH JOIN				 |			     |
     |	     |		  |	     |

|  27 |      INDEX FAST FULL SCAN		 | LOC_CITY_IX		     |
  23 |	 276 |	   1   (0)| 00:00:01 |

|  28 |      INDEX FAST FULL SCAN		 | LOC_ID_PK		     |
  23 |	 276 |	   1   (0)| 00:00:01 |

--------------------------------------------------------------------------------
--------------------------------------


Predicate Information (identified by operation id):
---------------------------------------------------

   8 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
   9 - access("D"."DEPARTMENT_ID"="HSB"."DEPARTMENT_ID")
  10 - access("D"."DEPARTMENT_ID"="ASB"."DEPARTMENT_ID")
  11 - filter("ASB"."AVG_SALARY"= (SELECT MAX("AVG_SALARY") FROM  (SELECT /*+ CA
CHE ("T1") */ "C0"

	      "DEPARTMENT_ID","C1" "AVG_SALARY" FROM "SYS"."SYS_TEMP_0FD9D6610_2
7D759" "T1") "AVG_SALARY_BY_DEPT"))

  17 - access(ROWID=ROWID)
  20 - filter("HSB"."HIGHEST_SALARY"= (SELECT MAX("HIGHEST_SALARY") FROM  (SELEC
T /*+ CACHE ("T1") */ "C0"

	      "DEPARTMENT_ID","C1" "HIGHEST_SALARY","C2" "HIGHEST_EMPLOYEE_ID" F
ROM "SYS"."SYS_TEMP_0FD9D6611_27D759" "T1")

	      "HIGHEST_SALARY_BY_DEPT"))
  26 - access(ROWID=ROWID)


统计信息
----------------------------------------------------------
	 64  recursive calls
	  0  db block gets
	 86  consistent gets
	  0  physical reads
	  0  redo size
	811  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  2  sorts (memory)
	  0  sorts (disk)
	  1  rows processed
```

## 比较两种查询方式的优劣

第一种方法使用了一个单独的SELECT语句进行了查询，而第二种方法使用了JOIN语句来联接两个表格。从性能方面来看，第二种方法可能更优。


# 优化建议

## 第一种查询方式


```python
GENERAL INFORMATION SECTION
-------------------------------------------------------------------------------
Tuning Task Name   : staName60580
Tuning Task Owner  : HR
Tuning Task ID     : 61
Workload Type      : Single SQL Statement
Execution Count    : 1
Current Execution  : EXEC_81
Execution Type     : TUNE SQL
Scope              : COMPREHENSIVE
Time Limit(seconds): 1800
Completion Status  : COMPLETED
Started at         : 03/16/2023 18:49:20
Completed at       : 03/16/2023 18:49:21

-------------------------------------------------------------------------------
Schema Name   : HR
Container Name: PDBORCL
SQL ID        : 20ratf479j3bk
SQL Text      : SELECT 
                  d.DEPARTMENT_ID, 
                  MAX(e.EMPLOYEE_ID) AS HIGHEST_EMPLOYEE_ID, 
                  MAX(e.SALARY) AS HIGHEST_SALARY, 
                  l.CITY
                FROM 
                  EMPLOYEES e 
                  INNER JOIN DEPARTMENTS d ON e.DEPARTMENT_ID =
                d.DEPARTMENT_ID 
                  INNER JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID 
                GROUP BY 
                  d.DEPARTMENT_ID, 
                  l.CITY 
                HAVING 
                  AVG(e.SALARY) = (
                    SELECT MAX(avg_salary)
                    FROM (
                      SELECT AVG(SALARY) AS avg_salary
                      FROM EMPLOYEES
                      GROUP BY DEPARTMENT_ID
                    )
                  )

-------------------------------------------------------------------------------
There are no recommendations to improve the statement.

-------------------------------------------------------------------------------
EXPLAIN PLANS SECTION
-------------------------------------------------------------------------------

1- Original
-----------
Plan hash value: 1241347288

 
-----------------------------------------------------------------------------------------------
| Id  | Operation                  | Name             | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT           |                  |     2 |    60 |     8  (13)| 00:00:01 |
|*  1 |  FILTER                    |                  |       |       |            |          |
|   2 |   HASH GROUP BY            |                  |     2 |    60 |     8  (13)| 00:00:01 |
|*  3 |    HASH JOIN               |                  |   106 |  3180 |     7   (0)| 00:00:01 |
|*  4 |     HASH JOIN              |                  |    27 |   513 |     4   (0)| 00:00:01 |
|   5 |      VIEW                  | index$_join$_004 |    23 |   276 |     2   (0)| 00:00:01 |
|*  6 |       HASH JOIN            |                  |       |       |            |          |
|   7 |        INDEX FAST FULL SCAN| LOC_CITY_IX      |    23 |   276 |     1   (0)| 00:00:01 |
|   8 |        INDEX FAST FULL SCAN| LOC_ID_PK        |    23 |   276 |     1   (0)| 00:00:01 |
|   9 |      VIEW                  | index$_join$_002 |    27 |   189 |     2   (0)| 00:00:01 |
|* 10 |       HASH JOIN            |                  |       |       |            |          |
|  11 |        INDEX FAST FULL SCAN| DEPT_ID_PK       |    27 |   189 |     1   (0)| 00:00:01 |
|  12 |        INDEX FAST FULL SCAN| DEPT_LOCATION_IX |    27 |   189 |     1   (0)| 00:00:01 |
|  13 |     TABLE ACCESS FULL      | EMPLOYEES        |   107 |  1177 |     3   (0)| 00:00:01 |
|  14 |   SORT AGGREGATE           |                  |     1 |    13 |            |          |
|  15 |    VIEW                    |                  |    11 |   143 |     4  (25)| 00:00:01 |
|  16 |     SORT GROUP BY          |                  |    11 |    77 |     4  (25)| 00:00:01 |
|  17 |      TABLE ACCESS FULL     | EMPLOYEES        |   107 |   749 |     3   (0)| 00:00:01 |
-----------------------------------------------------------------------------------------------
 
Query Block Name / Object Alias (identified by operation id):
-------------------------------------------------------------
 
   1 - SEL$9E43CB6E
   5 - SEL$7CCC7DEB / L@SEL$2
   6 - SEL$7CCC7DEB
   7 - SEL$7CCC7DEB / indexjoin$_alias$_001@SEL$7CCC7DEB
   8 - SEL$7CCC7DEB / indexjoin$_alias$_002@SEL$7CCC7DEB
   9 - SEL$751E8472 / D@SEL$1
  10 - SEL$751E8472
  11 - SEL$751E8472 / indexjoin$_alias$_001@SEL$751E8472
  12 - SEL$751E8472 / indexjoin$_alias$_002@SEL$751E8472
  13 - SEL$9E43CB6E / E@SEL$1
  14 - SEL$4       
  15 - SEL$5        / from$_subquery$_006@SEL$4
  16 - SEL$5       
  17 - SEL$5        / EMPLOYEES@SEL$5
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   1 - filter(SUM("E"."SALARY")/COUNT("E"."SALARY")= (SELECT MAX("AVG_SALARY") FROM  
              (SELECT SUM("SALARY")/COUNT("SALARY") "AVG_SALARY" FROM "EMPLOYEES" "EMPLOYEES" GROUP 
              BY "DEPARTMENT_ID") "from$_subquery$_006"))
   3 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
   4 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
   6 - access(ROWID=ROWID)
  10 - access(ROWID=ROWID)
 
Column Projection Information (identified by operation id):
-----------------------------------------------------------
 
   1 - "D"."DEPARTMENT_ID"[NUMBER,22], "L"."CITY"[VARCHAR2,30], MAX("E"."SALARY")[22], 
       MAX("E"."EMPLOYEE_ID")[22]
   2 - (#keys=2) "D"."DEPARTMENT_ID"[NUMBER,22], "L"."CITY"[VARCHAR2,30], 
       COUNT("E"."SALARY")[22], SUM("E"."SALARY")[22], MAX("E"."SALARY")[22], 
       MAX("E"."EMPLOYEE_ID")[22]
   3 - (#keys=1) "D"."DEPARTMENT_ID"[NUMBER,22], "L"."CITY"[VARCHAR2,30], 
       "E"."EMPLOYEE_ID"[NUMBER,22], "E"."SALARY"[NUMBER,22], "E"."EMPLOYEE_ID"[NUMBER,22], 
       "E"."SALARY"[NUMBER,22]
   4 - (#keys=1) "L"."CITY"[VARCHAR2,30], "D"."DEPARTMENT_ID"[NUMBER,22]
   5 - (rowset=256) "L"."LOCATION_ID"[NUMBER,22], "L"."CITY"[VARCHAR2,30]
   6 - (#keys=1; rowset=256) "L"."CITY"[VARCHAR2,30], "L"."LOCATION_ID"[NUMBER,22]
   7 - ROWID[ROWID,10], "L"."CITY"[VARCHAR2,30]
   8 - ROWID[ROWID,10], "L"."LOCATION_ID"[NUMBER,22]
   9 - (rowset=256) "D"."LOCATION_ID"[NUMBER,22], "D"."DEPARTMENT_ID"[NUMBER,22]
  10 - (#keys=1; rowset=256) "D"."DEPARTMENT_ID"[NUMBER,22], 
       "D"."LOCATION_ID"[NUMBER,22]
  11 - ROWID[ROWID,10], "D"."DEPARTMENT_ID"[NUMBER,22]
  12 - ROWID[ROWID,10], "D"."LOCATION_ID"[NUMBER,22]
  13 - "E"."EMPLOYEE_ID"[NUMBER,22], "E"."SALARY"[NUMBER,22], 
       "E"."DEPARTMENT_ID"[NUMBER,22]
  14 - (#keys=0) MAX("AVG_SALARY")[22]
  15 - (rowset=256) "AVG_SALARY"[NUMBER,22]
  16 - (#keys=1; rowset=256) "DEPARTMENT_ID"[NUMBER,22], COUNT("SALARY")[22], 
       SUM("SALARY")[22]
  17 - (rowset=256) "SALARY"[NUMBER,22], "DEPARTMENT_ID"[NUMBER,22]
 
Note
-----
   - this is an adaptive plan

-------------------------------------------------------------------------------

```

## 第二种查询方式


```python
GENERAL INFORMATION SECTION
-------------------------------------------------------------------------------
Tuning Task Name   : staName60893
Tuning Task Owner  : HR
Tuning Task ID     : 71
Workload Type      : Single SQL Statement
Execution Count    : 1
Current Execution  : EXEC_91
Execution Type     : TUNE SQL
Scope              : COMPREHENSIVE
Time Limit(seconds): 1800
Completion Status  : COMPLETED
Started at         : 03/16/2023 18:51:40
Completed at       : 03/16/2023 18:51:41

-------------------------------------------------------------------------------
Schema Name   : HR
Container Name: PDBORCL
SQL ID        : 1kvu1c3jggv4f
SQL Text      : WITH avg_salary_by_dept AS (
                  SELECT 
                    e.DEPARTMENT_ID, 
                    AVG(e.SALARY) AS avg_salary
                  FROM 
                    EMPLOYEES e 
                  GROUP BY 
                    e.DEPARTMENT_ID
                ),
                highest_salary_by_dept AS (
                  SELECT 
                    e.DEPARTMENT_ID, 
                    MAX(e.SALARY) AS highest_salary,
                    MAX(e.EMPLOYEE_ID) KEEP (DENSE_RANK FIRST ORDER BY
                e.SALARY DESC) AS highest_employee_id
                  FROM 
                    EMPLOYEES e 
                  GROUP BY 
                    e.DEPARTMENT_ID
                )
                SELECT 
                  d.DEPARTMENT_ID, 
                  hsb.highest_employee_id, 
                  hsb.highest_salary, 
                  l.CITY
                FROM 
                  DEPARTMENTS d 
                  INNER JOIN avg_salary_by_dept asb ON d.DEPARTMENT_ID =
                asb.DEPARTMENT_ID 
                  INNER JOIN LOCATIONS l ON d.LOCATION_ID = l.LOCATION_ID 
                  INNER JOIN highest_salary_by_dept hsb ON d.DEPARTMENT_ID =
                hsb.DEPARTMENT_ID AND hsb.highest_salary = (
                    SELECT MAX(highest_salary)
                    FROM highest_salary_by_dept
                  )
                WHERE 
                  asb.avg_salary = (
                    SELECT MAX(avg_salary)
                    FROM avg_salary_by_dept
                  )

-------------------------------------------------------------------------------
There are no recommendations to improve the statement.

-------------------------------------------------------------------------------
EXPLAIN PLANS SECTION
-------------------------------------------------------------------------------

1- Original
-----------
Plan hash value: 1583722651

 
----------------------------------------------------------------------------------------------------------------------
| Id  | Operation                                | Name                      | Rows  | Bytes | Cost (%CPU)| Time     |
----------------------------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT                         |                           |     9 |   756 |    20  (10)| 00:00:01 |
|   1 |  TEMP TABLE TRANSFORMATION               |                           |       |       |            |          |
|   2 |   LOAD AS SELECT (CURSOR DURATION MEMORY)| SYS_TEMP_0FD9D6613_2C80DB |       |       |            |          |
|   3 |    HASH GROUP BY                         |                           |    11 |    77 |     4  (25)| 00:00:01 |
|   4 |     TABLE ACCESS FULL                    | EMPLOYEES                 |   107 |   749 |     3   (0)| 00:00:01 |
|   5 |   LOAD AS SELECT (CURSOR DURATION MEMORY)| SYS_TEMP_0FD9D6614_2C80DB |       |       |            |          |
|   6 |    SORT GROUP BY                         |                           |    11 |   121 |     4  (25)| 00:00:01 |
|   7 |     TABLE ACCESS FULL                    | EMPLOYEES                 |   107 |  1177 |     3   (0)| 00:00:01 |
|*  8 |   HASH JOIN                              |                           |     9 |   756 |     8   (0)| 00:00:01 |
|*  9 |    HASH JOIN                             |                           |     9 |   648 |     6   (0)| 00:00:01 |
|* 10 |     HASH JOIN                            |                           |    10 |   330 |     4   (0)| 00:00:01 |
|* 11 |      VIEW                                |                           |    11 |   286 |     2   (0)| 00:00:01 |
|  12 |       TABLE ACCESS FULL                  | SYS_TEMP_0FD9D6613_2C80DB |    11 |    77 |     2   (0)| 00:00:01 |
|  13 |       SORT AGGREGATE                     |                           |     1 |    13 |            |          |
|  14 |        VIEW                              |                           |    11 |   143 |     2   (0)| 00:00:01 |
|  15 |         TABLE ACCESS FULL                | SYS_TEMP_0FD9D6613_2C80DB |    11 |    77 |     2   (0)| 00:00:01 |
|  16 |      VIEW                                | index$_join$_003          |    27 |   189 |     2   (0)| 00:00:01 |
|* 17 |       HASH JOIN                          |                           |       |       |            |          |
|  18 |        INDEX FAST FULL SCAN              | DEPT_ID_PK                |    27 |   189 |     1   (0)| 00:00:01 |
|  19 |        INDEX FAST FULL SCAN              | DEPT_LOCATION_IX          |    27 |   189 |     1   (0)| 00:00:01 |
|* 20 |     VIEW                                 |                           |    11 |   429 |     2   (0)| 00:00:01 |
|  21 |      TABLE ACCESS FULL                   | SYS_TEMP_0FD9D6614_2C80DB |    11 |   121 |     2   (0)| 00:00:01 |
|  22 |      SORT AGGREGATE                      |                           |     1 |    13 |            |          |
|  23 |       VIEW                               |                           |    11 |   143 |     2   (0)| 00:00:01 |
|  24 |        TABLE ACCESS FULL                 | SYS_TEMP_0FD9D6614_2C80DB |    11 |   121 |     2   (0)| 00:00:01 |
|  25 |    VIEW                                  | index$_join$_006          |    23 |   276 |     2   (0)| 00:00:01 |
|* 26 |     HASH JOIN                            |                           |       |       |            |          |
|  27 |      INDEX FAST FULL SCAN                | LOC_CITY_IX               |    23 |   276 |     1   (0)| 00:00:01 |
|  28 |      INDEX FAST FULL SCAN                | LOC_ID_PK                 |    23 |   276 |     1   (0)| 00:00:01 |
----------------------------------------------------------------------------------------------------------------------
 
Query Block Name / Object Alias (identified by operation id):
-------------------------------------------------------------
 
   1 - SEL$7FD93B7A
   2 - SEL$5       
   4 - SEL$5        / E@SEL$5
   5 - SEL$6       
   7 - SEL$6        / E@SEL$6
  11 - SEL$614025F9 / ASB@SEL$1
  12 - SEL$614025F9 / T1@SEL$614025F9
  13 - SEL$8       
  14 - SEL$614025F8 / AVG_SALARY_BY_DEPT@SEL$8
  15 - SEL$614025F8 / T1@SEL$614025F8
  16 - SEL$4D49BD76 / D@SEL$1
  17 - SEL$4D49BD76
  18 - SEL$4D49BD76 / indexjoin$_alias$_001@SEL$4D49BD76
  19 - SEL$4D49BD76 / indexjoin$_alias$_002@SEL$4D49BD76
  20 - SEL$BB173B70 / HSB@SEL$3
  21 - SEL$BB173B70 / T1@SEL$BB173B70
  22 - SEL$4       
  23 - SEL$BB173B6F / HIGHEST_SALARY_BY_DEPT@SEL$4
  24 - SEL$BB173B6F / T1@SEL$BB173B6F
  25 - SEL$335F8D5B / L@SEL$2
  26 - SEL$335F8D5B
  27 - SEL$335F8D5B / indexjoin$_alias$_001@SEL$335F8D5B
  28 - SEL$335F8D5B / indexjoin$_alias$_002@SEL$335F8D5B
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   8 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
   9 - access("D"."DEPARTMENT_ID"="HSB"."DEPARTMENT_ID")
  10 - access("D"."DEPARTMENT_ID"="ASB"."DEPARTMENT_ID")
  11 - filter("ASB"."AVG_SALARY"= (SELECT MAX("AVG_SALARY") FROM  (SELECT /*+ CACHE ("T1") */ "C0" 
              "DEPARTMENT_ID","C1" "AVG_SALARY" FROM "SYS"."SYS_TEMP_0FD9D6613_2C80DB" "T1") "AVG_SALARY_BY_DEPT"))
  17 - access(ROWID=ROWID)
  20 - filter("HSB"."HIGHEST_SALARY"= (SELECT MAX("HIGHEST_SALARY") FROM  (SELECT /*+ CACHE ("T1") */ "C0" 
              "DEPARTMENT_ID","C1" "HIGHEST_SALARY","C2" "HIGHEST_EMPLOYEE_ID" FROM "SYS"."SYS_TEMP_0FD9D6614_2C80DB" "T1") 
              "HIGHEST_SALARY_BY_DEPT"))
  26 - access(ROWID=ROWID)
 
Column Projection Information (identified by operation id):
-----------------------------------------------------------
 
   1 - "D"."LOCATION_ID"[NUMBER,22], "L"."LOCATION_ID"[NUMBER,22], "D"."DEPARTMENT_ID"[NUMBER,22], 
       "HSB"."DEPARTMENT_ID"[NUMBER,22], "ASB"."DEPARTMENT_ID"[NUMBER,22], "HSB"."HIGHEST_SALARY"[NUMBER,22], 
       "ASB"."AVG_SALARY"[NUMBER,22], ROWID[ROWID,10], "HSB"."HIGHEST_EMPLOYEE_ID"[NUMBER,22], ROWID[ROWID,10], 
       "L"."CITY"[VARCHAR2,30]
   2 - SYSDEF[4], SYSDEF[0], SYSDEF[1], SYSDEF[120], SYSDEF[0]
   3 - (#keys=1) "E"."DEPARTMENT_ID"[NUMBER,22], COUNT("E"."SALARY")[22], SUM("E"."SALARY")[22]
   4 - (rowset=256) "E"."SALARY"[NUMBER,22], "E"."DEPARTMENT_ID"[NUMBER,22]
   5 - SYSDEF[4], SYSDEF[0], SYSDEF[1], SYSDEF[120], SYSDEF[0]
   6 - (#keys=1) "E"."DEPARTMENT_ID"[NUMBER,22], MAX("E"."EMPLOYEE_ID") KEEP (DENSE_RANK FIRST  ORDER BY 
       INTERNAL_FUNCTION("E"."SALARY") DESC )[22], MAX("E"."SALARY")[22]
   7 - "E"."EMPLOYEE_ID"[NUMBER,22], "E"."SALARY"[NUMBER,22], "E"."DEPARTMENT_ID"[NUMBER,22]
   8 - (#keys=1) "D"."LOCATION_ID"[NUMBER,22], "L"."LOCATION_ID"[NUMBER,22], "D"."DEPARTMENT_ID"[NUMBER,22], 
       "HSB"."DEPARTMENT_ID"[NUMBER,22], "ASB"."DEPARTMENT_ID"[NUMBER,22], "HSB"."HIGHEST_SALARY"[NUMBER,22], 
       "ASB"."AVG_SALARY"[NUMBER,22], ROWID[ROWID,10], "HSB"."HIGHEST_EMPLOYEE_ID"[NUMBER,22], ROWID[ROWID,10], 
       "L"."CITY"[VARCHAR2,30]
   9 - (#keys=1; rowset=256) "D"."DEPARTMENT_ID"[NUMBER,22], "HSB"."DEPARTMENT_ID"[NUMBER,22], 
       "ASB"."DEPARTMENT_ID"[NUMBER,22], "D"."LOCATION_ID"[NUMBER,22], "ASB"."AVG_SALARY"[NUMBER,22], 
       ROWID[ROWID,10], "HSB"."HIGHEST_EMPLOYEE_ID"[NUMBER,22], "HSB"."HIGHEST_SALARY"[NUMBER,22]
  10 - (#keys=1; rowset=256) "ASB"."DEPARTMENT_ID"[NUMBER,22], "D"."DEPARTMENT_ID"[NUMBER,22], 
       "ASB"."AVG_SALARY"[NUMBER,22], ROWID[ROWID,10], "D"."LOCATION_ID"[NUMBER,22]
  11 - "ASB"."DEPARTMENT_ID"[NUMBER,22], "ASB"."AVG_SALARY"[NUMBER,22]
  12 - "C0"[NUMBER,22], "C1"[NUMBER,22]
  13 - (#keys=0) MAX("AVG_SALARY")[22]
  14 - (rowset=256) "AVG_SALARY"[NUMBER,22]
  15 - (rowset=256) "C0"[NUMBER,22], "C1"[NUMBER,22]
  16 - (rowset=256) ROWID[ROWID,10], "D"."LOCATION_ID"[NUMBER,22], "D"."DEPARTMENT_ID"[NUMBER,22]
  17 - (#keys=1; rowset=256) ROWID[ROWID,10], ROWID[ROWID,10], "D"."DEPARTMENT_ID"[NUMBER,22], 
       "D"."LOCATION_ID"[NUMBER,22]
  18 - ROWID[ROWID,10], "D"."DEPARTMENT_ID"[NUMBER,22]
  19 - ROWID[ROWID,10], "D"."LOCATION_ID"[NUMBER,22]
  20 - "HSB"."DEPARTMENT_ID"[NUMBER,22], "HSB"."HIGHEST_SALARY"[NUMBER,22], 
       "HSB"."HIGHEST_EMPLOYEE_ID"[NUMBER,22]
  21 - "C0"[NUMBER,22], "C1"[NUMBER,22], "C2"[NUMBER,22]
  22 - (#keys=0) MAX("HIGHEST_SALARY")[22]
  23 - (rowset=256) "HIGHEST_SALARY"[NUMBER,22]
  24 - (rowset=256) "C0"[NUMBER,22], "C1"[NUMBER,22], "C2"[NUMBER,22]
  25 - (rowset=256) ROWID[ROWID,10], "L"."LOCATION_ID"[NUMBER,22], "L"."CITY"[VARCHAR2,30]
  26 - (#keys=1; rowset=256) ROWID[ROWID,10], ROWID[ROWID,10], "L"."CITY"[VARCHAR2,30], 
       "L"."LOCATION_ID"[NUMBER,22]
  27 - ROWID[ROWID,10], "L"."CITY"[VARCHAR2,30]
  28 - ROWID[ROWID,10], "L"."LOCATION_ID"[NUMBER,22]

-------------------------------------------------------------------------------
```


```python

```
