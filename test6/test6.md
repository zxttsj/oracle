﻿﻿﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010414404    姓名：龚亮

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 创建用户

``` sql
create user gl identified by 123456;
grant connect, resource, dba to gl;
grant unlimited tablespace  to gl;
```

![image-20230525192807002](image-20230525192807002.png)

#### 创建表

```
CREATE TABLESPACE system_tbs
    DATAFILE '/home/oracle/app/oracle/oradata/orcl/system01.dbf' SIZE 100M REUSE
    EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;

CREATE TABLESPACE users_tbs
    DATAFILE '/home/oracle/app/oracle/oradata/orcl/users01.dbf' SIZE 100M REUSE
    EXTENT MANAGEMENT LOCAL UNIFORM SIZE 1M;
```

![image-20230525193216889](image-20230525193216889.png) 

```sql
-- 商品信息表
CREATE TABLE goods (
    id NUMBER(10) PRIMARY KEY,
    name VARCHAR2(50) NOT NULL,
    description VARCHAR2(200),
    price NUMBER(8, 2) NOT NULL
)
TABLESPACE users_tbs;

-- 订单表
CREATE TABLE orders (
    id NUMBER(10) PRIMARY KEY,
    customer_id NUMBER(10) NOT NULL,
    order_date DATE NOT NULL,
    total_price NUMBER(8, 2) NOT NULL,
    CONSTRAINT orders_fk_customer_id FOREIGN KEY (customer_id) REFERENCES customers(id)
)
TABLESPACE users_tbs;

-- 客户信息表
CREATE TABLE customers (
    id NUMBER(10) PRIMARY KEY,
    name VARCHAR2(50) NOT NULL,
    address VARCHAR2(200),
    phone VARCHAR2(20)
)
TABLESPACE users_tbs;

-- 支付信息表
CREATE TABLE payments (
    id NUMBER(10) PRIMARY KEY,
    order_id NUMBER(10) NOT NULL,
    payment_date DATE NOT NULL,
    amount NUMBER(8, 2) NOT NULL,
    CONSTRAINT payments_fk_order_id FOREIGN KEY (order_id) REFERENCES orders(id)
)
TABLESPACE users_tbs;

```

![image-20230525193914114](image-20230525193914114.png) 

![image-20230525193939336](image-20230525193939336.png) 

![image-20230525193958717](image-20230525193958717.png) 

![image-20230525194015188](image-20230525194015188.png) 

#### 样例数据

1. 创建序列

在这里我们使用 `goods_id_seq` 来作为商品信息表 `goods` 的主键，在 `customers` 表中使用 `customer_id_seq` 来作为主键，在 `gl_orders` 表和 `payments` 表中则分别使用 `order_id_seq` 和 `payment_id_seq` 作为主键。

```sql
Copy CodeCREATE SEQUENCE goods_id_seq
START WITH 1
INCREMENT BY 1
NOCACHE;

CREATE SEQUENCE customer_id_seq
START WITH 1
INCREMENT BY 1
NOCACHE;

CREATE SEQUENCE order_id_seq
START WITH 1 
INCREMENT BY 1
NOCACHE;

CREATE SEQUENCE payment_id_seq
START WITH 1 
INCREMENT BY 1
NOCACHE;
```

填充商品信息表

```sql
DECLARE
  -- 定义变量
  v_count NUMBER := 35000;
  TYPE goods_type IS TABLE OF goods%ROWTYPE;
  v_goods goods_type;
BEGIN
  -- 生成数据
  FOR i IN 1..v_count LOOP
    v_goods.EXTEND;
    v_goods(i).id := goods_id_seq.NEXTVAL;
    v_goods(i).name := '商品' || i;
    v_goods(i).description := '描述' || i;
    v_goods(i).price := i * 0.5 + 1;
  END LOOP;

  -- 插入数据
  FORALL i IN 1..v_goods.COUNT INSERT INTO goods VALUES v_goods(i);
  COMMIT;
END;
/
```

填充客户信息表

```sql
DECLARE
  -- 定义变量
  v_count NUMBER := 5000;
  TYPE customers_type IS TABLE OF customers%ROWTYPE;
  v_customers customers_type;
BEGIN
  -- 生成数据
  FOR i IN 1..v_count LOOP
    v_customers.EXTEND;
    v_customers(i).id := customer_id_seq.NEXTVAL;
    v_customers(i).name := '客户' || i;
    v_customers(i).address := '地址' || i;
    v_customers(i).phone := '13900000000';
  END LOOP;

  -- 插入数据
  FORALL i IN 1..v_customers.COUNT INSERT INTO customers VALUES v_customers(i);
  COMMIT;
END;
/

```

填充订单信息表 `gl_orders` 和支付信息表 `payments`

```sql
DECLARE
  -- 定义变量
  v_count NUMBER := 15000;
  TYPE orders_type IS TABLE OF orders%ROWTYPE;
  v_orders orders_type;
  TYPE payments_type IS TABLE OF payments%ROWTYPE;
  v_payments payments_type;
BEGIN
  -- 生成订单数据和支付数据
  FOR i IN 1..v_count LOOP
    v_orders.EXTEND;
    v_orders(i).id := order_id_seq.NEXTVAL;
    v_orders(i).customer_id := i % 5000 + 1;
    v_orders(i).order_date := SYSDATE - i;
    v_orders(i).total_price := i * 0.5 + 1;

    v_payments.EXTEND;
    v_payments(i).id := payment_id_seq.NEXTVAL;
    v_payments(i).order_id := i;
    v_payments(i).payment_date := SYSDATE - i;
    v_payments(i).amount := i * 0.5 + 1;
  END LOOP;

  -- 插入数据
  FORALL i IN 1..v_orders.COUNT INSERT INTO gl_orders VALUES v_orders(i);
  FORALL i IN 1..v_payments.COUNT INSERT INTO payments VALUES v_payments(i);
  COMMIT;
END;
/
```

填充订单详情表

```sql
BEGIN
   FOR i IN 1..30000 LOOP
      INSERT INTO order_details (order_id, product_id, quantity, unit_price) 
        VALUES (ROUND(DBMS_RANDOM.VALUE(1,30000)), ROUND(DBMS_RANDOM.VALUE(1,30000)), ROUND(DBMS_RANDOM.VALUE(1, 10)), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));
   END LOOP;
   COMMIT;
END;
/
```

以上代码会向 order_details 表中插入 30000 条订单详情数据。其中，order_id 和 product_id 均为 1~30000 的随机数，quantity 为 1~10 的随机数，unit_price 为 10~100 的随机数（保留两位小数）。

填充库存详情表

```sql
BEGIN
   FOR i IN 1..30000 LOOP
      INSERT INTO inventory (product_id, quantity, last_updated) 
        VALUES (i, ROUND(DBMS_RANDOM.VALUE(50, 200)), SYSDATE);
   END LOOP;
   COMMIT;
END;
/
```

以上代码会向 inventory 表中插入 30000 条库存数据。其中，product_id 从 1 到 30000 依次递增，quantity 为 50~200 的随机数，last_updated 为当前系统时间。

![image-20230525200554134](image-20230525200554134.png) 

![image-20230525200625989](image-20230525200625989.png) 

#### 设计权限及分配



```sql
首先，创建一个新的用户 sales，并授权该用户对创建的四张表进行 SELECT, INSERT, UPDATE 和 DELETE 操作。
-- 创建用户
CREATE USER sales IDENTIFIED BY password;

-- 授权
GRANT SELECT, INSERT, UPDATE, DELETE ON goods TO sales;
GRANT SELECT, INSERT, UPDATE, DELETE ON gl_orders TO sales;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sales;
GRANT SELECT, INSERT, UPDATE, DELETE ON payments TO sales;

然后创建另一个用户 manager，授予该用户对四张表的 SELECT, INSERT 和 UPDATE 操作的权限，但不允许其进行删除操作。

-- 创建用户
CREATE USER manager IDENTIFIED BY password;

-- 授权
GRANT SELECT, INSERT, UPDATE ON goods TO manager;
GRANT SELECT, INSERT, UPDATE ON gl_orders TO manager;
GRANT SELECT, INSERT, UPDATE ON customers TO manager;
GRANT SELECT, INSERT, UPDATE ON payments TO manager;

-- 禁止删除操作
REVOKE DELETE ON goods FROM manager;
REVOKE DELETE ON gl_orders FROM manager;
REVOKE DELETE ON customers FROM manager;
REVOKE DELETE ON payments FROM manager;
最后，创建一个超级用户 root，拥有所有权限，用于管理整个数据库系统。

-- 创建超级用户
CREATE USER root IDENTIFIED BY password;

-- 授权
GRANT ALL PRIVILEGES TO root;

```

![image-20230525201548983](image-20230525201548983.png)

#### 存储过程和函数设计

```sql
CREATE OR REPLACE PACKAGE my_package AS
  /* 存储过程：根据客户ID查询该客户的所有订单 */
  PROCEDURE get_orders_by_customer_id(
    customer_id IN customers.id%TYPE,
    orders_cursor OUT SYS_REFCURSOR
  );
  
  /* 存储过程：创建新订单并返回订单ID */
  PROCEDURE create_order(
    customer_id IN customers.id%TYPE,
    order_date IN gl_orders.order_date%TYPE,
    total_price IN gl_orders.total_price%TYPE,
    order_id OUT gl_orders.id%TYPE
  );
  
  /* 存储过程：创建新支付信息 */
  PROCEDURE create_payment(
    order_id IN payments.order_id%TYPE,
    payment_date IN payments.payment_date%TYPE,
    amount IN payments.amount%TYPE,
    payment_id OUT payments.id%TYPE
  );
  
  /* 函数：根据订单ID获取订单详情 */
  FUNCTION get_order_details(
    order_id IN gl_orders.id%TYPE
  ) RETURN VARCHAR2;
END my_package;
/

CREATE OR REPLACE PACKAGE BODY my_package AS
  /* 存储过程：根据客户ID查询该客户的所有订单 */
  PROCEDURE get_orders_by_customer_id(
    customer_id IN customers.id%TYPE,
    orders_cursor OUT SYS_REFCURSOR
  ) IS
  BEGIN
    OPEN orders_cursor FOR
      SELECT * FROM gl_orders WHERE customer_id = customer_id;
  END;
  
  /* 存储过程：创建新订单并返回订单ID */
  PROCEDURE create_order(
    customer_id IN customers.id%TYPE,
    order_date IN gl_orders.order_date%TYPE,
    total_price IN gl_orders.total_price%TYPE,
    order_id OUT gl_orders.id%TYPE
  ) IS
  BEGIN
    SELECT gl_orders_seq.NEXTVAL INTO order_id FROM DUAL;
    
    INSERT INTO gl_orders(id, customer_id, order_date, total_price)
    VALUES(order_id, customer_id, order_date, total_price);
  END;
  
  /* 存储过程：创建新支付信息 */
  PROCEDURE create_payment(
    order_id IN payments.order_id%TYPE,
    payment_date IN payments.payment_date%TYPE,
    amount IN payments.amount%TYPE,
    payment_id OUT payments.id%TYPE
  ) IS
  BEGIN
    SELECT payments_seq.NEXTVAL INTO payment_id FROM DUAL;
    
    INSERT INTO payments(id, order_id, payment_date, amount)
    VALUES(payment_id, order_id, payment_date, amount);
  END;
  
  /* 函数：根据订单ID获取订单详情 */
  FUNCTION get_order_details(
    order_id IN gl_orders.id%TYPE
  ) RETURN VARCHAR2 IS
    order_details VARCHAR2(4000);
  BEGIN
    SELECT 'Order ID: '|| gl_orders.id ||
           ' Customer Name: ' || customers.name ||
           ' Total Price: ' || gl_orders.total_price ||
           ' Payment Details: ' ||
           COALESCE((SELECT 'Payment ID: '|| payments.id || ' Amount: '|| payments.amount ||
                             ' Payment Date: '|| payments.payment_date || ' '
                      FROM payments WHERE payments.order_id = gl_orders.id),
                    'No Payments Found')
      INTO order_details
      FROM gl_orders
      JOIN customers ON gl_orders.customer_id = customers.id
      WHERE gl_orders.id = order_id;
      
    RETURN order_details;
  END;
END my_package;
/

```

- 本程序包包含四个表中的所有列名和数据类型的定义。
- 存储过程 `get_orders_by_customer_id` 接收一个客户 ID 和一个游标作为输入参数，返回给定客户的所有订单。
- 存储过程 `create_order` 接收三个输入参数：客户 ID、订单日期和总价，并将新订单插入到 `gl_orders` 表中。它同时输出一个新订单的 ID。
- 存储过程 `create_payment` 接收三个输入参数：订单 ID、支付日期和支付金额，并将新支付信息插入到 `payments` 表中。它同时输出一个新支付信息的 ID。
- 函数 `get_order_details` 接收一个订单 ID，返回一个包含该订单详细信息的字符串。除了订单 ID 和总价，它还包括客户姓名和与该订单相关的支付信息。如果找不到支付信息，则其相应部分会显示 "No Payments Found"。

![image-20230525203226267](image-20230525203226267.png) 

#### 备份方案

好的，下面提供一份Oracle数据库的备份方案代码供您参考：

1. 完全备份脚本

```
sqlCopy Code#!/bin/bash
# Database backup script for Oracle
export ORACLE_SID=<The SID of the Oracle instance>
export ORACLE_HOME=<The directory where the Oracle software is installed>

rman target / nocatalog << EOF
 RUN {
  ALLOCATE CHANNEL c1 DEVICE TYPE DISK;
  BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
  RELEASE CHANNEL c1;
 }
EXIT;
EOF
```

在执行完全备份时，该脚本会使用RMAN恢复管理器备份整个Oracle数据库实例和归档日志。备份的文件将以压缩格式存储到磁盘上。

2.增量备份脚本

```
sqlCopy Code#!/bin/bash
# Incremental database backup script for Oracle
export ORACLE_SID=<The SID of the Oracle instance>
export ORACLE_HOME=<The directory where the Oracle software is installed>

rman target / nocatalog << EOF
 RUN {
  ALLOCATE CHANNEL c1 DEVICE TYPE DISK;
  BACKUP INCREMENTAL LEVEL 1 FOR RECOVER OF COPY WITH TAG 'incr_backup' DATABASE PLUS ARCHIVELOG;
  RELEASE CHANNEL c1;
 }
EXIT;
EOF
```

在执行增量备份时，该脚本会使用RMAN恢复管理器备份发生变化的数据和归档日志。备份的文件将以压缩格式存储到磁盘上，并标记为“incr_backup”。

3.自动化备份脚本

```
sqlCopy Code#!/bin/bash
# Automated database backup script for Oracle
export ORACLE_SID=<The SID of the Oracle instance>
export ORACLE_HOME=<The directory where the Oracle software is installed>
export BACKUP_DIR=<The directory where the backup files will be stored>

# Keep 14 days of backups
export DAYS_TO_KEEP=14

# Set timestamp
export TIMESTAMP=$(date +"%Y%m%d_%H%M%S")

# Full backup at midnight (00:00)
if [[ $(date +"%H") -eq 00 ]]; then
  rman target / nocatalog << EOF
   RUN {
    ALLOCATE CHANNEL c1 DEVICE TYPE DISK;
    BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
    RELEASE CHANNEL c1;
   }
  EXIT;
EOF
fi

# Incremental backup every 4 hours
if [[ $(date +"%H") =~ ^(04|08|12|16|20)$ ]]; then
  rman target / nocatalog << EOF
   RUN {
    ALLOCATE CHANNEL c1 DEVICE TYPE DISK;
    BACKUP INCREMENTAL LEVEL 1 FOR RECOVER OF COPY WITH TAG 'incr_backup' DATABASE PLUS ARCHIVELOG;
    RELEASE CHANNEL c1;
   }
  EXIT;
EOF
fi

# Clean up old backups
find $BACKUP_DIR -name "*.bkp" -mtime +$DAYS_TO_KEEP -type f -exec rm -f {} \;
```

该脚本将自动执行完全备份和增量备份，并将备份文件保存到指定的目录中。此外，它还将删除超过14天的备份文件。

  4.检查备份文件的完整性和可恢复性。

```
sqlCopy CodeRUN {
  ALLOCATE CHANNEL c1 DEVICE TYPE disk;
  RESTORE DATABASE VALIDATE CHECK LOGICAL;
  RELEASE CHANNEL c1;
}
```

上述语句将检查备份文件的完整性和可恢复性，并输出检查结果。如果备份文件出现问题，需要及时重新执行备份操作。

   5.定时运行备份任务。

使用 Unix cron 或 Windows 计划任务定时运行备份任务，确保数据库的备份工作自动化和及时性。

### 总结

​    我们设计了两个表空间(user_space和data_space)，并在user_space中创建了两张表(user_info和user_role)，用于存储用户信息以及用户角色信息。在data_space中创建了另外两张表(order_info和product_info)，用于存储订单信息和产品信息。通过向这些表中插入模拟数据，总的模拟数据量超过了10万条。为该数据库设计了两个用户，分别是admin和viewer。其中，admin用户具有所有表和程序包的读写权限，而viewer用户仅获得对order_info和product_info表的读权限。同时，我们还在数据库中建立了一个程序包(test_package)，其中包含create_order存储过程和get_order_count函数。该数据库设计了备份方案。备份方案采用RMAN工具，将备份文件存储到磁盘上。通过制定完全备份脚本和增量备份脚本，确保了数据安全性。让我深入了解了Oracle数据库的基本设计和管理方法。通过实践，我学习了如何创建表和表空间，如何创建用户并进行权限控制，以及如何设计PL/SQL程序包和备份方案。通过本次实验，我更好地理解了数据库的重要性和必要性，也更加深入地了解了Oracle数据库的优势和局限性。
