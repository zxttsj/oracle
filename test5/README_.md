# 实验5：包，过程，函数的用法

姓名:龚亮 学号：202010414404

## 实验目的

了解PL/SQL语言结构
了解PL/SQL变量和常量的声明和使用方法
学习包，过程，函数的用法

## 实验内容


```python
以hr用户登录
创建一个包(Package)，包名是MyPack。
在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：
```


```python
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID

```


```python
create or replace PACKAGE MyPack IS

FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/

```

![](1.png)


```python
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;


```

![](2.png)


```python
过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;

```

![](4.png)

本次实验主要目的是了解PL/SQL语言结构、变量和常量的声明和使用方法，以及包、过程和函数的用法。在实验过程中，我们以hr用户登录为前提条件，通过创建一个名为MyPack的包，在其中创建了一个名为Get_SalaryAmount的函数和一个名为GET_EMPLOYEES的过程。

Get_SalaryAmount函数的功能是通过查询员工表，统计每个部门的工资总额。该函数接受一个部门ID作为输入参数，通过查询employees表来获取每个部门的工资总额。在查询过程中，我们使用了SUM函数和GROUP BY子句来计算每个部门的工资总额。

GET_EMPLOYEES过程的功能是递归查询某个员工及其所有下属、子下属员工。在该过程中，我们使用了游标和Oracle递归查询语句来实现。递归查询语句的格式为：SELECT LEVEL, EMPLOYEE_ID, FIRST_NAME, MANAGER_ID FROM employees START WITH EMPLOYEE_ID = V_EMPLOYEE_ID CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID。

总的来说，本次实验让我更深入地了解了PL/SQL语言和Oracle数据库的基本知识，包括语法结构、变量和常量的使用、包、过程和函数的定义和使用等。通过本次实验，我也更加熟悉了Oracle递归查询语句的用法和实现方法。


```python

```
